<?php
// Initialize the session
session_start();
// Include config file
require_once "config.php";
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
} else {
    try{
        $customerpdo = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_CUSTOMER, DB_CUSTOMER_USERNAME, DB_CUSTOMER_PASSWORD);
        $customerpdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $employeepdo = new PDO("mysql:host=" . DB_SERVER . ";dbname=" . DB_EMPLOYEE, DB_EMPLOYEE_USERNAME, DB_EMPLOYEE_PASSWORD);
        $employeepdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $e){
        die("ERROR: Could not connect. " . $e->getMessage());
    }
    $customer_stmt = $customerpdo->query("SELECT * FROM products");    
    $employee_stmt = $employeepdo->query("SELECT * FROM products");    
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
        table, th, td {
          border: 1px solid black;
          border-collapse: collapse;
        }
        img {
          width: 50%;
          height: auto;
        }
table.center {
  margin-left: auto; 
  margin-right: auto;
}
    </style>
</head>
<body>
    <h1 class="my-5">Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>. Welcome to our site.</h1>
    <h1 class="my-7">Product Data from Purple DB</h1>
    <table cellpadding="15" cellspacing="30" border="1" class="center">
        <tr>
            <th width="90">Sr. No</th>
            <th width="100">Image</th>
            <th width="200">Product Code</th>
            <th width="200">Name</th>
            <th width="200">Quantity</th>
            <th width="200">Price</th>
        </tr>
        <?php
        while ($custrow = $customer_stmt->fetch()) {
        ?>
            <tr>
                <td><?php echo $custrow['productID']; ?></td>
                <td><img src="../customer/<?php echo $custrow['productimg'];?>" alt="Product Image"></td>
                <td><?php echo $custrow['productCode']; ?></td>
                <td><?php echo $custrow['name']; ?></td>
                <td><?php echo $custrow['quantity']; ?></td>
                <td><?php echo $custrow['price']; ?></td>
            </tr>
        <?php
        }
        ?>
    </table>
    <br>
    <br>
    <h1 class="my-7">Product Data from Blue DB</h1>
    <table cellpadding="15" cellspacing="30" border="1" class="center">
        <tr>
            <th width="90">Sr. No</th>
            <th width="100">Image</th>
            <th width="200">Product Code</th>
            <th width="200">Name</th>
            <th width="200">Quantity</th>
            <th width="200">Price</th>
        </tr>
        <?php
        while ($emprow = $employee_stmt->fetch()) {
        ?>
            <tr>
                <td><?php echo $emprow['productID']; ?></td>
                <td><img src="../employee/<?php echo $emprow['productimg'];?>" alt="Product Image"></td>
                <td><?php echo $emprow['productCode']; ?></td>
                <td><?php echo $emprow['name']; ?></td>
                <td><?php echo $emprow['quantity']; ?></td>
                <td><?php echo $emprow['price']; ?></td>
            </tr>
        <?php
        }
        ?>
    </table>
    <p style="margin-top: 100px">
        <a href="logout.php" class="btn btn-danger ml-3">Sign Out of Your Account</a>
    </p>
</body>
</html>